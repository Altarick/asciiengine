#include "commonMath.h"

inline int nearModulo(unsigned char v)
{
    if (v == 0)
    {
        return 0;
    }
    else if (v < 95)
    {
        return 1;
    }
    else if (v < 135)
    {
        return 2;
    }
    else if (v < 175)
    {
        return 3;
    }
    else if (v < 215)
    {
        return 4;
    }
    else 
    {
        return 5;
    }
}

int Color::getID()
{
    return (36*nearModulo(r)) + (6*nearModulo(g)) + nearModulo(b) + 16;
}

int Color::getPairID(Color fg, Color bg)
{
    return (216*(fg.getID()-16)) + (bg.getID()-16) + 1 ;
}