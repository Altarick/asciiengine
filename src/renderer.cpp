#include "renderer.h"


Renderer::Renderer(WINDOW* window) :
    m_window(window),
    m_displayXSize(0),
    m_displayYSize(0),
    m_displayXSizeHalf(0),
    m_displayYSizeHalf(0),
    m_tempX(0),
    m_tempY(0),
    m_resizeFrameCounter(0),
    m_redArray(nullptr),
    m_greenArray(nullptr),
    m_blueArray(nullptr)
{
    if (has_colors())
    {
        start_color();
        int pairID = 1;

        for (size_t i = 16; i < 232; i++)
        {
            for (size_t j = 16; j < 232; j++)
            {
                init_extended_pair(pairID,i,j);
                pairID += 1;
            }
        }
    }
    else
    {
        std::cerr<< "Terminal is not 256-color compatible, aborting ..." << std::endl;
        exit(1);
    }
    resizeArrays();
}

Renderer::~Renderer()
{
    delete m_redArray;
    delete m_greenArray;
    delete m_blueArray;
}

ArrayPoint Renderer::projectToArray(float x, float y)
{
    size_t rx = (x+1.f)*m_displayXSizeHalf;
    size_t ry = (y+1.f)*m_displayYSizeHalf;
    return ArrayPoint(rx,ry);
}

void Renderer::resizeArrays()
{
    getmaxyx(m_window,m_displayYSize,m_displayXSize);
    m_displayYSize *= 2;
    m_displayXSizeHalf = ((float) m_displayXSize)*0.5;
    m_displayYSizeHalf = ((float) m_displayYSize)*0.5;

    delete m_redArray;
    delete m_greenArray;
    delete m_blueArray;

    m_redArray = new unsigned char[m_displayXSize*m_displayYSize];
    m_greenArray = new unsigned char[m_displayXSize*m_displayYSize];
    m_blueArray = new unsigned char[m_displayXSize*m_displayYSize];
}


void Renderer::updateDisplay()
{
    Color tmpColor;
    attr_t atr;
    short tmppair;
    attr_get(&atr,&tmppair,nullptr);
    for (size_t i = 0; i < m_displayXSize; i++)
    {
        for (size_t j = 0; j < m_displayYSize-1; j += 2)
        {
            attr_set(atr,Color::getPairID({m_redArray[at(i,j)],m_greenArray[at(i,j)],m_blueArray[at(i,j)]},{m_redArray[at(i,j+1)],m_greenArray[at(i,j+1)],m_blueArray[at(i,j+1)]}),nullptr);
            
            mvaddstr(j/2,i,"\u2580");
        }
    }

    
    

    refresh();

    size_t tmpX;
    size_t tmpY;
    getmaxyx(m_window,tmpY,tmpX);
    //here we need to manage resize
    if(tmpX != m_displayXSize || tmpY != m_displayYSize) //the window's size doesn't match current
    {
        if(tmpX != m_tempX || tmpY != m_tempY) //the window's size hasn't settled
        {
            m_resizeFrameCounter = 0;
            m_tempX = tmpX;
            m_tempY = tmpY;
        }
        else
        {
            m_resizeFrameCounter += 1;
            if(m_resizeFrameCounter > 30)
            {
                m_resizeFrameCounter = 0;
                m_displayXSize = m_tempX;
                m_displayYSize = m_tempY;
                resizeArrays();
            }
        }
    }

}

void Renderer::fillScreen(Color c)
{
    for (size_t i = 0; i < (size_t) (m_displayXSize*m_displayYSize); i++)
    {
        m_redArray[i] = c.r;
        m_greenArray[i] = c.g;
        m_blueArray[i] = c.b;
    }
}

void Renderer::drawPoint(Point2D p,Color c)
{
    ArrayPoint projectedPoint = projectToArray(p);
    if(isInsideArray(projectedPoint))
    {
        size_t index = at(projectedPoint);
        m_redArray[index] = c.r;
        m_greenArray[index] = c.g;
        m_blueArray[index] = c.b;
    }
}

/**
 * @brief Draws a line using Bresenham's algorithm
 * 
 * @param p1 
 * @param c1 
 * @param p2 
 * @param c2 
 */
void Renderer::drawLine(Point2D p1,Color c1, Point2D p2, Color c2)
{
    ArrayPoint a1 = projectToArray(p1);
    ArrayPoint a2 = projectToArray(p2);

    //inverting color if perfect vertical since it draws the line in the opposite direction in this case
    Color& ca = a1.x == a2.x ? c2 : c1;
    Color& cb = a1.x == a2.x ? c1 : c2;

    int dx = a2.x - a1.x;
    int sx;
    if( a1.x < a2.x)
    {
        sx = 1;
    }
    else
    {
        sx = -1;
        dx = 0-dx;
    }
    int dy = a2.y - a1.y;
    int sy;
    if( a1.y < a2.y)
    {
        sy = 1;
        dy = 0-dy;
    }
    else
    {
        sy = -1;
    }
    int err = dx+dy;
    int e2;

    int pixelLength = dx > -dy ? dx : dy;
    int increment = 0;
    float incrementDiv = pixelLength == 0 ? 0 : 1 / ((float) pixelLength) ;


    while (true)
    {
        m_redArray[at(a1.x,a1.y)] = ((float) ca.r)*(1.f-(incrementDiv*increment)) + ((float) cb.r)*(incrementDiv*increment);
        m_greenArray[at(a1.x,a1.y)] = ((float) ca.g)*(1.f-(incrementDiv*increment)) + ((float) cb.g)*(incrementDiv*increment);
        m_blueArray[at(a1.x,a1.y)] = ((float) ca.b)*(1.f-(incrementDiv*increment)) + ((float) cb.b)*(incrementDiv*increment);

        increment += 1;

        if (a1.x == a2.x && a1.y == a2.y) break;
        e2 = 2*err;
        if (e2 >= dy)
        {
            err += dy;
            a1.x += sx;
        }
        if (e2 <= dx)
        {
            err += dx;
            a1.y += sy;
        }
        
        
    }
}