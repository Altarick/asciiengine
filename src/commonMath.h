#ifndef MATH_H
#define MATH_H


#include<iostream>

class Color
{
    public:
        Color(): r(0),g(0),b(0) {;}
        Color(unsigned char cr,unsigned char cg,unsigned char cb): r(cr),g(cg),b(cb) {;}

        unsigned char r;
        unsigned char g;
        unsigned char b;

        //returns the ID of the color
        int getID();

        static int getPairID(Color foreground,Color background);
};

class ArrayPoint
{
    public :
        ArrayPoint(): x(0),y(0) {;}
        ArrayPoint(int px, int py): x(px),y(py) {;}

        int x;
        int y;
        
};

class Point2D
{
    public :
        Point2D(): x(0),y(0) {;}
        Point2D(float px, float py): x(px),y(py) {;}

        float x;
        float y;
};


class Point3D
{
    public :
        Point3D(): x(0),y(0),z(0) {;}
        Point3D(float px, float py, float pz): x(px),y(py),z(pz) {;}

        float x;
        float y;
        float z;

};



#endif