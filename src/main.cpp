#include <stdlib.h>
#include <signal.h>
#include <locale.h>
#include <thread>
#include <chrono>
#include "renderer.h"

static void finish(int sig);

int main()
{
    (void) signal(SIGINT, finish);      /* arrange interrupts to terminate */

    setlocale(LC_ALL, ""); /*set the local so we can use utf8 blocks*/
    WINDOW* mainWindow = initscr();      
    keypad(stdscr, TRUE);  /* enable keyboard mapping */
    (void) cbreak();       /* take input chars one at a time, no wait for \n */
    (void) noecho();       /* don't echo input */
    curs_set(0);

    Renderer rend(mainWindow);
    
    float pos = -1;

    while (true)
    {
        rend.fillScreen(Color(255,255,255));


        rend.drawPoint(Point2D(pos,0),Color(128,0,0));
        pos += 0.01;
        if(pos>1) pos = -1;

        rend.drawLine(Point2D(0,0),Color(0,255,0),Point2D(0,+.8),Color(0,0,255));
        rend.drawLine(Point2D(0,0),Color(0,255,0),Point2D(0,-.8),Color(0,0,255));
        rend.drawLine(Point2D(0,0),Color(0,255,0),Point2D(+.8,0),Color(0,0,255));
        rend.drawLine(Point2D(0,0),Color(0,255,0),Point2D(-.8,0),Color(0,0,255));
        
        rend.drawLine(Point2D(0,0),Color(0,255,0),Point2D(+.8,+.8),Color(0,0,255));
        rend.drawLine(Point2D(0,0),Color(0,255,0),Point2D(-.8,+.8),Color(0,0,255));
        rend.drawLine(Point2D(0,0),Color(0,255,0),Point2D(+.8,-.8),Color(0,0,255));
        rend.drawLine(Point2D(0,0),Color(0,255,0),Point2D(-.8,-.8),Color(0,0,255));
        
        
        rend.updateDisplay();

        std::this_thread::sleep_for(std::chrono::milliseconds(34));
    }
    

    finish(0);               /* we are done */
}

static void finish(int )
{
    endwin();

    /* do your non-curses wrapup here */

    exit(0);
}

