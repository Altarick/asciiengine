#ifndef RENDERER_H
#define RENDERER_H

#include <stdlib.h>
#include <curses.h>
#include <signal.h>
#include <locale.h>

#include "commonMath.h"

class Renderer
{
    public:
        Renderer(WINDOW* window);
        ~Renderer();

        void updateDisplay();

        //display functions
        void fillScreen(Color);
        void drawPoint(Point2D,Color);
        void drawLine(Point2D,Color,Point2D,Color);
        void drawLineSmooth(Point2D,Color,Point2D,Color);
        void drawTriangle(Point2D,Color,Point2D,Color,Point2D,Color);
        void drawTriangleSmooth(Point2D,Color,Point2D,Color,Point2D,Color);
        void drawRectangle(Point2D,Color,Point2D,Color,Point2D,Color,Point2D,Color);
        void drawRectangleSmooth(Point2D,Color,Point2D,Color,Point2D,Color,Point2D,Color);

    private:

        WINDOW* m_window;

        //window size management
        size_t m_displayXSize;
        size_t m_displayYSize;
        float m_displayXSizeHalf;
        float m_displayYSizeHalf;

        size_t m_tempX;
        size_t m_tempY;
        uint m_resizeFrameCounter;

        size_t at(size_t x,size_t y) { return (y*m_displayXSize) + x ;}
        size_t at(ArrayPoint p) { return at(p.x,p.y);}
        bool isInsideArray(int x,int y) { return x >= 0  && y >= 0 && x < (int) m_displayXSize && y < (int) m_displayYSize ;}
        bool isInsideArray(ArrayPoint p) { return isInsideArray(p.x,p.y);}

        ArrayPoint projectToArray(float x, float y);
        ArrayPoint projectToArray(Point2D p) {return projectToArray(p.x,p.y);}

        unsigned char* m_redArray;
        unsigned char* m_greenArray;
        unsigned char* m_blueArray;

        void resizeArrays();

        
        
};


#endif